﻿namespace FASS.API.Model;

public class Utilisateur
{
    public int UtilisateurId { get; set; }
    public string Prenom { get; set; }
    public string Nom { get; set; }
    public int NiveauAcces { get; set; }
    public string Profession { get; set; }
    public string MotDePasse { get; set; }
}