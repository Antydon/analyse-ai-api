﻿namespace FASS.API.Model;

public class Patient
{
    public int PatientId { get; set; }
    public string Prenom { get; set; }
    public string Nom { get; set; }
    public int Age { get; set; }
    public string ConditionMedicale { get; set; }
    public int CodeAssurance { get; set; }
    public string Telephone { get; set; }
    public string Courriel { get; set; }
    public bool Texto { get; set; }
    public string Ordonnance { get; set; }
    public int CliniqueId { get; set; }
    public int PharmacieId { get; set; }
}
