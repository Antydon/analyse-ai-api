﻿namespace FASS.API.Model;

public class Pharmacie
{
    public int PharmacieId { get; set; }
    public string Nom { get; set; }
    public string Adresse { get; set; }
    public string Telephone { get; set; }
}