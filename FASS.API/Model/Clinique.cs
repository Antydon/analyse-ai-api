﻿namespace FASS.API.Model;

public class Clinique
{
    public int CliniqueId { get; set; }
    public string Nom { get; set; }
    public string Adresse { get; set; }
    public string Telephone { get; set; }
}
