﻿namespace FASS.API.Model;

public class Medicament
{
    public int MedicamentId { get; set; }
    public string Nom { get; set; }
    public string Type { get; set; }
    public string Effets { get; set; }
    public string Instructions { get; set; }
}
