﻿namespace FASS.API.Model;

public class Symptome
{
    public int SymptomeId { get; set; }
    public string Nom { get; set; }
    public string Type { get; set; }
    public string ConditionDiagnostic { get; set; }

    public ICollection<MaladieSymptome>? MaladieSymptomes { get; set; } = new List<MaladieSymptome>();

}
