﻿namespace FASS.API.Model;

public class Ordonnance
{
    public int OrdonnanceId { get; set; }
    public int MedicamentId { get; set; }
    public int PrescripteurId { get; set; }
    public int DureePriseMedication { get; set; }
    public DateTime DatePrescrit { get; set; }
    public DateTime DateExpiration { get; set; }
    public bool Urgence { get; set; }
    public int PatientId { get; set; }
}
