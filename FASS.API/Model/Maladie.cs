﻿namespace FASS.API.Model;

public class Maladie
{
    public int MaladieId { get; set; }
    public string Nom { get; set; }
    public string Type { get; set; }
    public string ZonesAffectes { get; set; }

    public ICollection<MaladieSymptome>? MaladieSymptomes { get; set; } = new List<MaladieSymptome>();
}
