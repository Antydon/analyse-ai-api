﻿namespace FASS.API.Model;

public class MaladieSymptome
{
    public int MaladieId { get; set; }

    public Maladie Maladie { get; set; }
    public int SymptomeId { get; set; }

    public Symptome Symptome { get; set; }
}
