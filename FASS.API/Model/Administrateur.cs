﻿namespace FASS.API.Model;

public class Administrateur
{
    public int AdministrateurId { get; set; }
    public string Nom { get; set; }
    public string Prenom { get; set; }
    public string Autorisation { get; set; }
    public string Profession { get; set; }
    public string MotDePasse { get; set; }
}