﻿using FASS.API.Model;
using FASS.API.Repositories.Context;
using Microsoft.EntityFrameworkCore;

namespace FASS.API.Repositories;

public class CliniqueRepository : ICliniqueRepository
{
    private readonly PrescriptionContext _context;

    public CliniqueRepository(PrescriptionContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Clinique>> GetAllCliniques()
    {
        return await _context.Set<Clinique>().ToListAsync();
    }

    public async Task<Clinique> GetCliniqueById(int id)
    {
        return await _context.Set<Clinique>().FindAsync(id);
    }

    public async Task<Clinique> CreateClinique(Clinique clinique)
    {
        _context.Set<Clinique>().Add(clinique);
        await _context.SaveChangesAsync();
        return clinique;
    }

    public async Task<Clinique> UpdateClinique(int id, Clinique clinique)
    {
        var existingClinique = await _context.Set<Clinique>().FindAsync(id);
        if (existingClinique == null)
            return null;

        existingClinique.Nom = clinique.Nom;
        // Update other properties as needed

        await _context.SaveChangesAsync();
        return existingClinique;
    }

    public async Task<bool> DeleteClinique(int id)
    {
        var clinique = await _context.Set<Clinique>().FindAsync(id);
        if (clinique == null)
            return false;

        _context.Set<Clinique>().Remove(clinique);
        await _context.SaveChangesAsync();
        return true;
    }
}
