﻿using FASS.API.Model;

namespace FASS.API.Repositories;
public interface IUtilisateurRepository
{
    Task<Utilisateur> Create(Utilisateur entity);
    Task<bool> Delete(int id);
    Task<IEnumerable<Utilisateur>> GetAll();
    Task<Utilisateur> GetById(int id);
    Task<Utilisateur> Update(int id, Utilisateur entity);
}