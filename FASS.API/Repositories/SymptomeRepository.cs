﻿namespace FASS.API.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using FASS.API.Model;
using FASS.API.Repositories.Context;
using Microsoft.EntityFrameworkCore;

public class SymptomeRepository : ISymptomeRepository
{
    private readonly PrescriptionContext _context;

    public SymptomeRepository(PrescriptionContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Symptome>> GetAllSymptomes()
    {
        return await _context.Set<Symptome>().ToListAsync();
    }

    public async Task<Symptome> GetSymptomeById(int id)
    {
        return await _context.Set<Symptome>().FindAsync(id);
    }

    public async Task<Symptome> CreateSymptome(Symptome symptome)
    {
        _context.Set<Symptome>().Add(symptome);
        await _context.SaveChangesAsync();
        return symptome;
    }

    public async Task<Symptome> UpdateSymptome(int id, Symptome symptome)
    {
        var existingSymptome = await _context.Set<Symptome>().FindAsync(id);
        if (existingSymptome == null)
            return null;

        existingSymptome.Nom = symptome.Nom;
        existingSymptome.Type = symptome.Type;
        // Update other properties as needed

        await _context.SaveChangesAsync();
        return existingSymptome;
    }

    public async Task<bool> DeleteSymptome(int id)
    {
        var symptome = await _context.Set<Symptome>().FindAsync(id);
        if (symptome == null)
            return false;

        _context.Set<Symptome>().Remove(symptome);
        await _context.SaveChangesAsync();
        return true;
    }
}

