﻿using FASS.API.Model;
using FASS.API.Repositories.Context;
using Microsoft.EntityFrameworkCore;

namespace FASS.API.Repositories;

public class MaladieRepository : IMaladieRepository
{
    private readonly PrescriptionContext _context;

    public MaladieRepository(PrescriptionContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Maladie>> GetAllMaladies()
    {
        return await _context.Set<Maladie>().ToListAsync();
    }

    public async Task<Maladie> GetMaladieById(int id)
    {
        return await _context.Set<Maladie>().FindAsync(id);
    }

    public async Task<Maladie> CreateMaladie(Maladie symptome)
    {
        _context.Set<Maladie>().Add(symptome);
        await _context.SaveChangesAsync();
        return symptome;
    }

    public async Task<Maladie> UpdateMaladie(int id, Maladie symptome)
    {
        var existingMaladie = await _context.Set<Maladie>().FindAsync(id);
        if (existingMaladie == null)
            return null;

        existingMaladie.Nom = symptome.Nom;
        existingMaladie.Type = symptome.Type;
        // Update other properties as needed

        await _context.SaveChangesAsync();
        return existingMaladie;
    }

    public async Task<bool> DeleteMaladie(int id)
    {
        var symptome = await _context.Set<Maladie>().FindAsync(id);
        if (symptome == null)
            return false;

        _context.Set<Maladie>().Remove(symptome);
        await _context.SaveChangesAsync();
        return true;
    }
}
