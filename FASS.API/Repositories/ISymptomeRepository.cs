﻿using FASS.API.Model;

namespace FASS.API.Repositories;
public interface ISymptomeRepository
{
    Task<Symptome> CreateSymptome(Symptome symptome);
    Task<bool> DeleteSymptome(int id);
    Task<IEnumerable<Symptome>> GetAllSymptomes();
    Task<Symptome> GetSymptomeById(int id);
    Task<Symptome> UpdateSymptome(int id, Symptome symptome);
}