﻿using FASS.API.Model;
using FASS.API.Repositories.Context;
using Microsoft.EntityFrameworkCore;

namespace FASS.API.Repositories;

public class PharmacieRepository : IPharmacieRepository
{
    private readonly PrescriptionContext _context;

    public PharmacieRepository(PrescriptionContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Pharmacie>> GetAllPharmacies()
    {
        return await _context.Set<Pharmacie>().ToListAsync();
    }

    public async Task<Pharmacie> GetPharmacieById(int id)
    {
        return await _context.Set<Pharmacie>().FindAsync(id);
    }

    public async Task<Pharmacie> CreatePharmacie(Pharmacie patient)
    {
        _context.Set<Pharmacie>().Add(patient);
        await _context.SaveChangesAsync();
        return patient;
    }

    public async Task<Pharmacie> UpdatePharmacie(int id, Pharmacie patient)
    {
        var existingPharmacie = await _context.Set<Pharmacie>().FindAsync(id);
        if (existingPharmacie == null)
            return null;

        existingPharmacie.Nom = patient.Nom;
        // Update other properties as needed

        await _context.SaveChangesAsync();
        return existingPharmacie;
    }

    public async Task<bool> DeletePharmacie(int id)
    {
        var patient = await _context.Set<Pharmacie>().FindAsync(id);
        if (patient == null)
            return false;

        _context.Set<Pharmacie>().Remove(patient);
        await _context.SaveChangesAsync();
        return true;
    }
}
