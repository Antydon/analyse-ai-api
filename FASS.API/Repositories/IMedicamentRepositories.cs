﻿using FASS.API.Model;

namespace FASS.API.Repositories;
public interface IMedicamentRepositories
{
    Task<Medicament> Create(Medicament entity);
    Task<bool> Delete(int id);
    Task<IEnumerable<Medicament>> GetAll();
    Task<Medicament> GetById(int id);
    Task<Medicament> Update(int id, Medicament entity);
}