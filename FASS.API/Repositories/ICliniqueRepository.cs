﻿using FASS.API.Model;

namespace FASS.API.Repositories;
public interface ICliniqueRepository
{
    Task<Clinique> CreateClinique(Clinique clinique);
    Task<bool> DeleteClinique(int id);
    Task<IEnumerable<Clinique>> GetAllCliniques();
    Task<Clinique> GetCliniqueById(int id);
    Task<Clinique> UpdateClinique(int id, Clinique clinique);
}