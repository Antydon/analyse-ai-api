﻿using FASS.API.Model;
using Microsoft.EntityFrameworkCore;

namespace FASS.API.Repositories.Context;

public class PrescriptionContext : DbContext
{
    /// <summary>
    /// Constructeur pour la migration
    /// </summary>
	public PrescriptionContext() : base()
    {

    }

    /// <summary>
    /// Constructeur pour l'utilisation en programme
    /// </summary>
    /// <param name="options">Option de la base de données</param>
    public PrescriptionContext(DbContextOptions<PrescriptionContext> options)
        : base(options)
    {
    }

#if DEBUG //Permet d'inclure cette méthode uniquement si l'application est en mode DEBUG
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //Vérifie si la configuration n'a pas été spécifiée par un fichier de configuration
        if (optionsBuilder.IsConfigured == false) {
            //Aucune configuration à partir d'un fichier de configuration
            //Option de base pour la migration                                     
            string? chaineConnexion = "Server=localhost\\SQLExpress;Database=FASS;Trusted_Connection=True;TrustServerCertificate=True;";

            //Vérifie si la variable n'est pas vide
            if (string.IsNullOrEmpty(chaineConnexion) == false) {
                //La variable n'est pas vide, la chaine de connexion est appliquée
                optionsBuilder.UseSqlServer(chaineConnexion);
            } else {
                //Il n'y a aucune chaine de connexion.
                throw new Exception("La variable MIGRATION_CONNECTION_STRING n'est pas spécifiée. Effectuez la commande suivante dans la Console du Gestionnaire de package : $env:MIGRATION_CONNECTION_STRING=\"[ma chaine de connexion]\" ");
            }
        }

    }
#endif

    /// <summary>
    /// Configuration spécifique de la base de données
    /// </summary>
    /// <param name="modelBuilder"></param>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<MaladieSymptome>(entity => {
            entity.ToTable("UtilisateurMusiqueTb");

            entity.HasOne(p => p.Maladie).WithMany(e => e.MaladieSymptomes)
                   .HasConstraintName("FK_MaladieSymptome_Maladie").IsRequired().OnDelete(DeleteBehavior.Cascade);

            entity.HasOne(p => p.Symptome).WithMany(e => e.MaladieSymptomes)
                  .HasConstraintName("FK_UtilisateurMusique_MusiqueId").IsRequired().OnDelete(DeleteBehavior.Cascade);

            entity.HasKey(p => new { p.MaladieId, p.SymptomeId });
        });
    }

    public DbSet<Medicament> MedicamentTb { get; set; }

    public DbSet<Utilisateur> UtilisateurTb { get; set; }

    public DbSet<Maladie> MaladieTb { get; set; }

    public DbSet<Clinique> CliniqueTb { get; set; }

    public DbSet<Administrateur> AdministrateurTb { get; set; }

    public DbSet<MaladieSymptome> MaladieSymptomeTb { get; set; }

    public DbSet<Ordonnance> OrdonnanceTb { get; set; }

    public DbSet<Patient> PatientTb { get; set; }

    public DbSet<Pharmacie> PharmacieTb { get; set; }

    public DbSet<Symptome> SymptomeTb { get; set; }
}
