﻿using FASS.API.Model;

namespace FASS.API.Repositories;
public interface IPharmacieRepository
{
    Task<Pharmacie> CreatePharmacie(Pharmacie patient);
    Task<bool> DeletePharmacie(int id);
    Task<IEnumerable<Pharmacie>> GetAllPharmacies();
    Task<Pharmacie> GetPharmacieById(int id);
    Task<Pharmacie> UpdatePharmacie(int id, Pharmacie patient);
}