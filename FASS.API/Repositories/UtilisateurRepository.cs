﻿using FASS.API.Model;
using FASS.API.Repositories.Context;
using Microsoft.EntityFrameworkCore;

namespace FASS.API.Repositories;

public class UtilisateurRepository : IUtilisateurRepository
{

    private readonly PrescriptionContext _context;

    public UtilisateurRepository(PrescriptionContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Utilisateur>> GetAll()
    {
        return await _context.Set<Utilisateur>().ToListAsync();
    }

    public async Task<Utilisateur> GetById(int id)
    {
        return await _context.Set<Utilisateur>().FindAsync(id);
    }

    public async Task<Utilisateur> Create(Utilisateur entity)
    {
        _context.Set<Utilisateur>().Add(entity);
        await _context.SaveChangesAsync();
        return entity;
    }

    public async Task<Utilisateur> Update(int id, Utilisateur entity)
    {
        _context.Entry(entity).State = EntityState.Modified;
        await _context.SaveChangesAsync();
        return entity;
    }

    public async Task<bool> Delete(int id)
    {
        var entity = await GetById(id);
        if (entity == null)
            return false;

        _context.Set<Utilisateur>().Remove(entity);
        await _context.SaveChangesAsync();
        return true;
    }
}
