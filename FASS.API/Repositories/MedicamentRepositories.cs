﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FASS.API.Model;
using FASS.API.Repositories.Context;

namespace FASS.API.Repositories;

public class MedicamentRepositories : IMedicamentRepositories
{
    private readonly PrescriptionContext _context;

    public MedicamentRepositories(PrescriptionContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Medicament>> GetAll()
    {
        return await _context.Set<Medicament>().ToListAsync();
    }

    public async Task<Medicament> GetById(int id)
    {
        return await _context.Set<Medicament>().FindAsync(id);
    }

    public async Task<Medicament> Create(Medicament entity)
    {
        _context.Set<Medicament>().Add(entity);
        await _context.SaveChangesAsync();
        return entity;
    }

    public async Task<Medicament> Update(int id, Medicament entity)
    {
        _context.Entry(entity).State = EntityState.Modified;
        await _context.SaveChangesAsync();
        return entity;
    }

    public async Task<bool> Delete(int id)
    {
        var entity = await GetById(id);
        if (entity == null)
            return false;

        _context.Set<Medicament>().Remove(entity);
        await _context.SaveChangesAsync();
        return true;
    }
}
