﻿using FASS.API.Model;

public interface IPatientRepository
{
    Task<Patient> CreatePatient(Patient patient);
    Task<bool> DeletePatient(int id);
    Task<IEnumerable<Patient>> GetAllPatients();
    Task<Patient> GetPatientById(int id);
    Task<Patient> UpdatePatient(int id, Patient patient);
}