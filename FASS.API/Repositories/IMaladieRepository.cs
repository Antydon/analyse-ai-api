﻿using FASS.API.Model;

namespace FASS.API.Repositories;
public interface IMaladieRepository
{
    Task<Maladie> CreateMaladie(Maladie symptome);
    Task<bool> DeleteMaladie(int id);
    Task<IEnumerable<Maladie>> GetAllMaladies();
    Task<Maladie> GetMaladieById(int id);
    Task<Maladie> UpdateMaladie(int id, Maladie symptome);
}