﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FASS.API.Model;
using FASS.API.Repositories.Context;
using Microsoft.EntityFrameworkCore;

public class PatientRepository : IPatientRepository
{
    private readonly PrescriptionContext _context;

    public PatientRepository(PrescriptionContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Patient>> GetAllPatients()
    {
        return await _context.Set<Patient>().ToListAsync();
    }

    public async Task<Patient> GetPatientById(int id)
    {
        return await _context.Set<Patient>().FindAsync(id);
    }

    public async Task<Patient> CreatePatient(Patient patient)
    {   
        _context.Set<Patient>().Add(patient);
        await _context.SaveChangesAsync();
        return patient;
    }

    public async Task<Patient> UpdatePatient(int id, Patient patient)
    {
        var existingPatient = await _context.Set<Patient>().FindAsync(id);
        if (existingPatient == null)
            return null;

        existingPatient.Prenom = patient.Prenom;
        existingPatient.Nom = patient.Nom;
        // Update other properties as needed

        await _context.SaveChangesAsync();
        return existingPatient;
    }

    public async Task<bool> DeletePatient(int id)
    {
        var patient = await _context.Set<Patient>().FindAsync(id);
        if (patient == null)
            return false;

        _context.Set<Patient>().Remove(patient);
        await _context.SaveChangesAsync();
        return true;
    }
}
