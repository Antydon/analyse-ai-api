using FASS.API;
using FASS.API.Repositories.Context;
using FASS.API.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddDbContext<PrescriptionContext>();
builder.Services.AddHttpContextAccessor();

// Add controllers
builder.Services.AddControllers();

// Add repositories
builder.Services.AddScoped<IMedicamentRepositories, MedicamentRepositories>();
builder.Services.AddScoped<IMaladieRepository, MaladieRepository>();
builder.Services.AddScoped<ICliniqueRepository, CliniqueRepository>();
builder.Services.AddScoped<IPatientRepository, PatientRepository>();
builder.Services.AddScoped<IPharmacieRepository, PharmacieRepository>();
builder.Services.AddScoped<IUtilisateurRepository, UtilisateurRepository>();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
