﻿using FASS.API.Repositories;
using FASS.API.Repositories.Context;

namespace FASS.API;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        // Add DbContext
        services.AddDbContext<PrescriptionContext>();
        services.AddHttpContextAccessor();

        // Add controllers
        services.AddControllers();

        // Add repositories
        services.AddScoped<IMedicamentRepositories, MedicamentRepositories>();
        services.AddScoped<IMaladieRepository, MaladieRepository>();
        services.AddScoped<ICliniqueRepository, CliniqueRepository>();
        services.AddScoped<IPatientRepository, PatientRepository>();
        services.AddScoped<IPharmacieRepository, PharmacieRepository>();
        services.AddScoped<IUtilisateurRepository, UtilisateurRepository>();

    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment()) {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();

        app.UseEndpoints(endpoints => {
            endpoints.MapControllers();
        });
    }
}
