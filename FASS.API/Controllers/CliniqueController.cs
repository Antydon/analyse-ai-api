﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FASS.API.Controllers;

using FASS.API.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using FASS.API.Repositories;

[ApiController]
[Route("api/[controller]")]
public class CliniqueController : ControllerBase
{
    private readonly ICliniqueRepository _repository;

    public CliniqueController(ICliniqueRepository repository)
    {
        _repository = repository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllCliniques()
    {
        var cliniques = await _repository.GetAllCliniques();
        return Ok(cliniques);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetCliniqueById(int id)
    {
        var clinique = await _repository.GetCliniqueById(id);
        if (clinique == null)
            return NotFound();

        return Ok(clinique);
    }

    [HttpPost]
    public async Task<IActionResult> CreateClinique(Clinique clinique)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var createdClinique = await _repository.CreateClinique(clinique);
        return CreatedAtAction(nameof(GetCliniqueById), new { id = createdClinique.CliniqueId }, createdClinique);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateClinique(int id, Clinique clinique)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var updatedClinique = await _repository.UpdateClinique(id, clinique);
        if (updatedClinique == null)
            return NotFound();

        return Ok(updatedClinique);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteClinique(int id)
    {
        var result = await _repository.DeleteClinique(id);
        if (!result)
            return NotFound();

        return NoContent();
    }
}
