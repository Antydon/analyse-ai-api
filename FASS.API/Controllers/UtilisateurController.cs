﻿using FASS.API.Model;
using FASS.API.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

[ApiController]
[Route("api/[controller]")]
public class UtilisateurController : ControllerBase
{
    private readonly IUtilisateurRepository _repository;

    public UtilisateurController(IUtilisateurRepository repository)
    {
        _repository = repository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllUtilisateurs()
    {
        var utilisateurs = await _repository.GetAll();
        return Ok(utilisateurs);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetUtilisateurById(int id)
    {
        var utilisateur = await _repository.GetById(id);
        if (utilisateur == null)
            return NotFound();

        return Ok(utilisateur);
    }

    [HttpPost]
    public async Task<IActionResult> CreateUtilisateur(Utilisateur utilisateur)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var createdUtilisateur = await _repository.Create(utilisateur);
        return CreatedAtAction(nameof(GetUtilisateurById), new { id = createdUtilisateur.UtilisateurId }, createdUtilisateur);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateUtilisateur(int id, Utilisateur utilisateur)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var updatedUtilisateur = await _repository.Update(id, utilisateur);
        if (updatedUtilisateur == null)
            return NotFound();

        return Ok(updatedUtilisateur);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteUtilisateur(int id)
    {
        var result = await _repository.Delete(id);
        if (!result)
            return NotFound();

        return NoContent();
    }
}
