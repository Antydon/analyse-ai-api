﻿using FASS.API.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

[ApiController]
[Route("api/[controller]")]
public class PatientController : ControllerBase
{
    private readonly IPatientRepository _repository;

    public PatientController(IPatientRepository repository)
    {
        _repository = repository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllPatients()
    {
        var patients = await _repository.GetAllPatients();
        return Ok(patients);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetPatientById(int id)
    {
        var patient = await _repository.GetPatientById(id);
        if (patient == null)
            return NotFound();

        return Ok(patient);
    }

    [HttpPost]
    public async Task<IActionResult> CreatePatient(Patient patient)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var createdPatient = await _repository.CreatePatient(patient);
        return CreatedAtAction(nameof(GetPatientById), new { id = createdPatient.PatientId }, createdPatient);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdatePatient(int id, Patient patient)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var updatedPatient = await _repository.UpdatePatient(id, patient);
        if (updatedPatient == null)
            return NotFound();

        return Ok(updatedPatient);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePatient(int id)
    {
        var result = await _repository.DeletePatient(id);
        if (!result)
            return NotFound();

        return NoContent();
    }
}
