﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FASS.API.Controllers;

using FASS.API.Model;
using FASS.API.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

[ApiController]
[Route("api/[controller]")]
public class PharmacieController : ControllerBase
{
    private readonly IPharmacieRepository _repository;

    public PharmacieController(IPharmacieRepository repository)
    {
        _repository = repository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllPharmacies()
    {
        var pharmacies = await _repository.GetAllPharmacies();
        return Ok(pharmacies);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetPharmacieById(int id)
    {
        var pharmacie = await _repository.GetPharmacieById(id);
        if (pharmacie == null)
            return NotFound();

        return Ok(pharmacie);
    }

    [HttpPost]
    public async Task<IActionResult> CreatePharmacie(Pharmacie pharmacie)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var createdPharmacie = await _repository.CreatePharmacie(pharmacie);
        return CreatedAtAction(nameof(GetPharmacieById), new { id = createdPharmacie.PharmacieId }, createdPharmacie);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdatePharmacie(int id, Pharmacie pharmacie)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var updatedPharmacie = await _repository.UpdatePharmacie(id, pharmacie);
        if (updatedPharmacie == null)
            return NotFound();

        return Ok(updatedPharmacie);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePharmacie(int id)
    {
        var result = await _repository.DeletePharmacie(id);
        if (!result)
            return NotFound();

        return NoContent();
    }
}
