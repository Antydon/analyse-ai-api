﻿namespace FASS.API.Controllers;

using FASS.API.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using FASS.API.Repositories;

[ApiController]
[Route("api/[controller]")]
public class MaladieController : ControllerBase
{
    private readonly IMaladieRepository _repository;

    public MaladieController(IMaladieRepository repository)
    {
        _repository = repository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllMaladies()
    {
        var maladies = await _repository.GetAllMaladies();
        return Ok(maladies);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetMaladieById(int id)
    {
        var maladie = await _repository.GetMaladieById(id);
        if (maladie == null)
            return NotFound();

        return Ok(maladie);
    }

    [HttpPost]
    public async Task<IActionResult> CreateMaladie(Maladie maladie)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var createdMaladie = await _repository.CreateMaladie(maladie);
        return CreatedAtAction(nameof(GetMaladieById), new { id = createdMaladie.MaladieId }, createdMaladie);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateMaladie(int id, Maladie maladie)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var updatedMaladie = await _repository.UpdateMaladie(id, maladie);
        if (updatedMaladie == null)
            return NotFound();

        return Ok(updatedMaladie);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteMaladie(int id)
    {
        var result = await _repository.DeleteMaladie(id);
        if (!result)
            return NotFound();

        return NoContent();
    }
}
