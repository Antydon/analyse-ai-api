﻿using FASS.API.Model;
using FASS.API.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

[ApiController]
[Route("api/[controller]")]
public class MedicamentController : ControllerBase
{
    private readonly IMedicamentRepositories _repository;

    public MedicamentController(IMedicamentRepositories repository)
    {
        _repository = repository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllMedicaments()
    {
        var medicaments = await _repository.GetAll();
        return Ok(medicaments);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetMedicamentById(int id)
    {
        var medicament = await _repository.GetById(id);
        if (medicament == null)
            return NotFound();

        return Ok(medicament);
    }

    [HttpPost]
    public async Task<IActionResult> CreateMedicament(Medicament medicament)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var createdMedicament = await _repository.Create(medicament);
        return CreatedAtAction(nameof(GetMedicamentById), new { id = createdMedicament.MedicamentId }, createdMedicament);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateMedicament(int id, Medicament medicament)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var updatedMedicament = await _repository.Update(id, medicament);
        if (updatedMedicament == null)
            return NotFound();

        return Ok(updatedMedicament);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteMedicament(int id)
    {
        var result = await _repository.Delete(id);
        if (!result)
            return NotFound();

        return NoContent();
    }
}