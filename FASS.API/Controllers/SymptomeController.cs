﻿using FASS.API.Model;
using FASS.API.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

[ApiController]
[Route("api/[controller]")]
public class SymptomeController : ControllerBase
{
    private readonly ISymptomeRepository _repository;

    public SymptomeController(ISymptomeRepository repository)
    {
        _repository = repository;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllSymptomes()
    {
        var symptomes = await _repository.GetAllSymptomes();
        return Ok(symptomes);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetSymptomeById(int id)
    {
        var symptome = await _repository.GetSymptomeById(id);
        if (symptome == null)
            return NotFound();

        return Ok(symptome);
    }

    [HttpPost]
    public async Task<IActionResult> CreateSymptome(Symptome symptome)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var createdSymptome = await _repository.CreateSymptome(symptome);
        return CreatedAtAction(nameof(GetSymptomeById), new { id = createdSymptome.SymptomeId }, createdSymptome);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateSymptome(int id, Symptome symptome)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var updatedSymptome = await _repository.UpdateSymptome(id, symptome);
        if (updatedSymptome == null)
            return NotFound();

        return Ok(updatedSymptome);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteSymptome(int id)
    {
        var result = await _repository.DeleteSymptome(id);
        if (!result)
            return NotFound();

        return NoContent();
    }
}
