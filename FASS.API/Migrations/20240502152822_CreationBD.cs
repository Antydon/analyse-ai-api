﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FASS.API.Migrations
{
    /// <inheritdoc />
    public partial class CreationBD : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdministrateurTb",
                columns: table => new
                {
                    AdministrateurId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Prenom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Autorisation = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Profession = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MotDePasse = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdministrateurTb", x => x.AdministrateurId);
                });

            migrationBuilder.CreateTable(
                name: "CliniqueTb",
                columns: table => new
                {
                    CliniqueId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Adresse = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CliniqueTb", x => x.CliniqueId);
                });

            migrationBuilder.CreateTable(
                name: "MaladieTb",
                columns: table => new
                {
                    MaladieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ZonesAffectes = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaladieTb", x => x.MaladieId);
                });

            migrationBuilder.CreateTable(
                name: "MedicamentTb",
                columns: table => new
                {
                    MedicamentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Effets = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Instructions = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicamentTb", x => x.MedicamentId);
                });

            migrationBuilder.CreateTable(
                name: "OrdonnanceTb",
                columns: table => new
                {
                    OrdonnanceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MedicamentId = table.Column<int>(type: "int", nullable: false),
                    PrescripteurId = table.Column<int>(type: "int", nullable: false),
                    DureePriseMedication = table.Column<int>(type: "int", nullable: false),
                    DatePrescrit = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateExpiration = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Urgence = table.Column<bool>(type: "bit", nullable: false),
                    PatientId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrdonnanceTb", x => x.OrdonnanceId);
                });

            migrationBuilder.CreateTable(
                name: "PatientTb",
                columns: table => new
                {
                    PatientId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Prenom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Age = table.Column<int>(type: "int", nullable: false),
                    ConditionMedicale = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CodeAssurance = table.Column<int>(type: "int", nullable: false),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Courriel = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Texto = table.Column<bool>(type: "bit", nullable: false),
                    Ordonnance = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CliniqueId = table.Column<int>(type: "int", nullable: false),
                    PharmacieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientTb", x => x.PatientId);
                });

            migrationBuilder.CreateTable(
                name: "PharmacieTb",
                columns: table => new
                {
                    PharmacieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Adresse = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PharmacieTb", x => x.PharmacieId);
                });

            migrationBuilder.CreateTable(
                name: "SymptomeTb",
                columns: table => new
                {
                    SymptomeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ConditionDiagnostic = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SymptomeTb", x => x.SymptomeId);
                });

            migrationBuilder.CreateTable(
                name: "UtilisateurTb",
                columns: table => new
                {
                    UtilisateurId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Prenom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NiveauAcces = table.Column<int>(type: "int", nullable: false),
                    Profession = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MotDePasse = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtilisateurTb", x => x.UtilisateurId);
                });

            migrationBuilder.CreateTable(
                name: "UtilisateurMusiqueTb",
                columns: table => new
                {
                    MaladieId = table.Column<int>(type: "int", nullable: false),
                    SymptomeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtilisateurMusiqueTb", x => new { x.MaladieId, x.SymptomeId });
                    table.ForeignKey(
                        name: "FK_MaladieSymptome_Maladie",
                        column: x => x.MaladieId,
                        principalTable: "MaladieTb",
                        principalColumn: "MaladieId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UtilisateurMusique_MusiqueId",
                        column: x => x.SymptomeId,
                        principalTable: "SymptomeTb",
                        principalColumn: "SymptomeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UtilisateurMusiqueTb_SymptomeId",
                table: "UtilisateurMusiqueTb",
                column: "SymptomeId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdministrateurTb");

            migrationBuilder.DropTable(
                name: "CliniqueTb");

            migrationBuilder.DropTable(
                name: "MedicamentTb");

            migrationBuilder.DropTable(
                name: "OrdonnanceTb");

            migrationBuilder.DropTable(
                name: "PatientTb");

            migrationBuilder.DropTable(
                name: "PharmacieTb");

            migrationBuilder.DropTable(
                name: "UtilisateurMusiqueTb");

            migrationBuilder.DropTable(
                name: "UtilisateurTb");

            migrationBuilder.DropTable(
                name: "MaladieTb");

            migrationBuilder.DropTable(
                name: "SymptomeTb");
        }
    }
}
